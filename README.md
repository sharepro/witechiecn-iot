﻿# witechiecn-iot

#### 介绍
1、“慧联云”平台借助于先进的物联网，传感技术，云计算，人工智能等技术，将生产现场、场区、中控系统等诸多数据接入云端服务器，对生产等各环节数据进行实时采集跟踪、展示、及分析、预警。并根据模型对远程的电器设备进行启停运行控制，实现“传感控制”。采集的数据类型AI量如：环境温度、湿度、压力、流量、电流、电压、振动、实时视频等。开关量如：启停信号。控制信号量DO的输出等。
2.  平台主要包含注册与登录、首页看板、汇聚感知、设备控制、报警管理、系统管理等六大功能模块。

#### 软件架构
1.  前端：PC端：Vue+Element,移动端：微信小程序，IOS ,Android。
2.  后端：Java/Mysql/Mqtt/Mongdb/Redis/Es
3.  底层接入支持：Modbus/Profibus/OPC/ZigBee/Ble/...
4.  传输层：Mqtt/CoAP/Websocke/Http/...

#### 安装教程

1.  云端部署/内网部署

#### 系统应用切图

1.  系统首页：首页看板，包含了接入总览、报警总览、资源消耗。
![输入图片说明](https://gitee.com/sharepro/internet-of-things-system/raw/master/1_20220725221450.png)
![输入图片说明](https://gitee.com/sharepro/internet-of-things-system/raw/master/6_20220725231703.png)
2.  报警管理：通过报警管理模块，对实时采集到的底层数据进行监控，如果满足报警规则，系统将产生报警记录，并同时进行短信、邮箱、微信等途径进行实时推送。支持复杂的多条件及参数规则定义。满足复杂场景需要。
![输入图片说明](https://gitee.com/sharepro/internet-of-things-system/raw/master/2_20220725222045.png)
![输入图片说明](https://gitee.com/sharepro/internet-of-things-system/raw/master/3_20220725222057.png)
3.  汇聚感知：分产品管理、设备监控、电器设备运转状态控制三块。
![输入图片说明](https://gitee.com/sharepro/internet-of-things-system/raw/master/4_20220725222116.png)
4.  系统设置提供了账号、组织架构、消息推送、接入授权等功能。
![输入图片说明](https://gitee.com/sharepro/internet-of-things-system/raw/master/5_20220725222134.png)
#### 产品联系

1.  业务合作：微信号：witechiecn ;邮箱：jack@witechie.cn;
2.  官网：http://www.witechie.cn/

#### 特别感谢

1.  感谢Gitee.com平台提供平台。
