/**
 * @author   156780583@qq.com （不想保留author可删除）
 * @description router全局配置，如有必要可分文件抽离，其中asyncRoutes只有在intelligence模式下才会用到，vip文档中已提供路由的基础图标与小清新图标的配置方案，请仔细阅读
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/layouts'
import EmptyLayout from '@/layouts/EmptyLayout'
import { publicPath, routerMode } from '@/config'

Vue.use(VueRouter)
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true,
  },
  {
    path: '/401',
    name: '401',
    component: () => import('@/views/401'),
    hidden: true,
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true,
  },
]

export const asyncRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    children: [
      {
        path: 'index',
        name: 'Index',
        component: () => import('@/views/index/index'),
        meta: {
          title: '首页',
          icon: 'home',
          affix: true,
        },
      },
    ],
  },
  {
    path: '/alarmmng',
    component: Layout,
    redirect: 'noRedirect',
    name: 'Alarmmng',
    meta: { title: '报警管理', icon: 'shopping-cart', permissions: ['admin'] },
    children: [
      {
        path: 'roleconfig',
        name: 'Roleconfig',
        component: () => import('@/views/alarmmng/roleconfig/index'),
        meta: { title: '报警配置' },
      },
      {
        path: 'alarmlog',
        name: 'Alarmlog',
        component: () => import('@/views/alarmmng/alarmlog/index'),
        meta: { title: '报警记录' },
      },
    ],
  },
  {
    path: '/gsensor',
    component: Layout,
    redirect: 'noRedirect',
    name: 'Gsensor',
    meta: { title: '汇聚感知', icon: 'bug', permissions: ['admin'] },
    children: [
      {
        path: 'product',
        name: 'Product',
        component: () => import('@/views/product/prdmng/index'),
        meta: { title: '产品' },
      },
      {
        path: 'productmng',
        name: 'Productmng',
        component: () => import('@/views/product/devmonitor/index'),
        meta: { title: '设备监控' },
      },
    ],
  },
  {
    path: '/system',
    component: Layout,
    redirect: 'noRedirect',
    name: 'System',
    meta: { title: '系统设置', icon: 'users-cog', permissions: ['admin'] },
    children: [
      {
        path: 'sysAcct',
        name: 'SysAcct',
        component: () => import('@/views/system/account/index'),
        meta: { title: '账号管理' },
      },
      {
        path: 'sysDept',
        name: 'SysDept',
        component: () => import('@/views/alarmmng/template/index'),
        meta: { title: '组织架构' },
      },
      {
        path: 'sysMessageType',
        name: 'SysMessageType',
        component: () => import('@/views/system/messagepush/index'),
        meta: { title: '消息推送' },
      },
      {
        path: 'productauth',
        name: 'productauth',
        component: () => import('@/views/product/prdmng/auth'),
        meta: { title: '接入授权' },
      },
    ],
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true,
  },
]

const router = new VueRouter({
  base: publicPath,
  mode: routerMode,
  scrollBehavior: () => ({
    y: 0,
  }),
  routes: constantRoutes,
})

export function resetRouter() {
  location.reload()
}

export default router
