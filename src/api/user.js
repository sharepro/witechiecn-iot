import request from '@/utils/request'
import { encryptedData } from '@/utils/encrypt'
import { loginRSA, tokenName } from '@/config'

export async function login(data) {
  if (loginRSA) {
    data = await encryptedData(data)
  }
  return request({
    url: '/Mobile/login',
    method: 'post',
    data,
  })
}

export function getUserInfo(accessToken) {
  return request({
    url: '/Mobile/userInfo',
    method: 'post',
    data: {
      [tokenName]: accessToken,
    },
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post',
  })
}

export function register(data) {
  return request({
    url: '/Acct/register',
    method: 'post',
    data,
  })
}

export function getVerifyCode(data) {
  return request({
    url: '/Acct/getVerifyCode',
    method: 'post',
    data,
  })
}

export async function verifyCodeCheck(data) {
  return request({
    url: '/Acct/verifyCodeCheck',
    method: 'post',
    data,
  })
}
