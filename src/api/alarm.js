import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/Alarm/getRoleList',
    method: 'post',
    data,
  })
}

export function doEdit(data) {
  return request({
    url: '/Alarm/saveRole',
    method: 'post',
    data,
  })
}

export function doDelete(data) {
  return request({
    url: '/Alarm/delrole',
    method: 'post',
    data,
  })
}

export function getAlarmLogList(data) {
  return request({
    url: '/Alarm/getAlarmLogList',
    method: 'post',
    data,
  })
}

export function saveAlarmReason(data) {
  return request({
    url: '/Alarm/saveAlarmReason',
    method: 'post',
    data,
  })
}

export function auditRole(data) {
  return request({
    url: '/Alarm/auditRole',
    method: 'post',
    data,
  })
}

export function getAlarmChart(data) {
  return request({
    url: '/Alarm/getAlarmChart',
    method: 'post',
    data,
  })
}

export function getSumary(data) {
  return request({
    url: '/Alarm/getSumary',
    method: 'post',
    data,
  })
}
