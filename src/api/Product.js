import request from '@/utils/request'

export function saveDevice(data) {
  return request({
    url: '/Product/saveDevice',
    method: 'post',
    data,
  })
}

export function saveProduct(data) {
  return request({
    url: '/Product/saveProduct',
    method: 'post',
    data,
  })
}

export function getProList(data) {
  return request({
    url: '/Product/getProList',
    method: 'post',
    data,
  })
}

export function getDevList(data) {
  return request({
    url: '/Product/getDevList',
    method: 'post',
    data,
  })
}

export function devDel(data) {
  return request({
    url: '/Product/devDel',
    method: 'post',
    data,
  })
}

export function loadProdevTree(data) {
  return request({
    url: '/Product/loadProdevTree',
    method: 'post',
    data,
  })
}

export function loadDiscoverPoint(data) {
  return request({
    url: '/Product/loadDiscoverPoint',
    method: 'post',
    data,
  })
}

export function loadProDevMonitor(data) {
  return request({
    url: '/Product/loadProDevMonitor',
    method: 'post',
    data,
  })
}

export function savePointCfg(data) {
  return request({
    url: '/Product/savePointCfgTagUnit',
    method: 'post',
    data,
  })
}

export function delPoint(data) {
  return request({
    url: '/Product/delPoint',
    method: 'post',
    data,
  })
}

export function saveSelecteRealPoint(data) {
  return request({
    url: '/Product/saveSelecteRealPoint',
    method: 'post',
    data,
  })
}

export function devControl(data) {
  return request({
    url: '/Product/devControl',
    method: 'post',
    data,
  })
}

export function getProductSeries(data) {
  return request({
    url: '/Product/getProductSeries',
    method: 'post',
    data,
  })
}

export function productAuth(data) {
  return request({
    url: '/Product/productAuth',
    method: 'post',
    data,
  })
}
