import request from '@/utils/request'

export function getDeviceMeasPointList(data) {
  return request({
    url: '/Factorymodel/getDeviceMeasPointList',
    method: 'post',
    data,
  })
}

export function deviceSaveMeasuringPoint(data) {
  return request({
    url: '/Factorymodel/deviceSaveMeasuringPoint',
    method: 'post',
    data,
  })
}

export function deviceMeasPointDel(data) {
  return request({
    url: '/Factorymodel/deviceMeasPointDel',
    method: 'post',
    data,
  })
}

export function saveNode(data) {
  return request({
    url: '/Factorymodel/saveNode',
    method: 'post',
    data,
  })
}

export function getNodeDetail(data) {
  return request({
    url: '/Factorymodel/getNodeDetail',
    method: 'post',
    data,
  })
}

export function nodeDel(data) {
  return request({
    url: '/Factorymodel/nodeDel',
    method: 'post',
    data,
  })
}

export function loadProDeviceTree(data) {
  return request({
    url: '/Factorymodel/loadProDeviceTree',
    method: 'post',
    data,
  })
}

export function loadDevicePointSource(data) {
  return request({
    url: '/Factorymodel/loadDevicePointSource',
    method: 'post',
    data,
  })
}

export function getDateUnit(data) {
  return request({
    url: '/Factorymodel/getDateUnit',
    method: 'post',
    data,
  })
}
