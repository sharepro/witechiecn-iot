import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/Acct/getAccList',
    method: 'post',
    data,
  })
}

export function doEdit(data) {
  return request({
    url: '/Acct/saveAccount',
    method: 'post',
    data,
  })
}

export function doDelete(data) {
  return request({
    url: '/Acct/delAccount',
    method: 'post',
    data,
  })
}

export function getUserDeptTree(data) {
  return request({
    url: '/Acct/getUserDeptTree',
    method: 'post',
    data,
  })
}

export function getApplyNodes(data) {
  return request({
    url: '/Biz/getApplyNodes',
    method: 'post',
    data,
  })
}

export function formInstanceDetailOpt(data) {
  return request({
    url: '/Biz/formInstanceDetailOpt',
    method: 'post',
    data,
  })
}

export function devControl(data) {
  return request({
    url: '/Product/devControl',
    method: 'post',
    data,
  })
}
