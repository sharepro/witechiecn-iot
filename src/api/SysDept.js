import request from '@/utils/request'

export function getDeptlist(data) {
  return request({
    url: '/Acct/getDeptlist',
    method: 'post',
    data,
  })
}

export function saveDept(data) {
  return request({
    url: '/Acct/saveDept',
    method: 'post',
    data,
  })
}

export function getDeptlistTree(data) {
  return request({
    url: '/Acct/getDeptlistTree',
    method: 'post',
    data,
  })
}
