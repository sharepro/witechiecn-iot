import request from '@/utils/request'

export function formList(data) {
  return request({
    url: '/Biz/formList',
    method: 'post',
    data,
  })
}

export function doEdit(data) {
  return request({
    url: '/Biz/addForm',
    method: 'post',
    data,
  })
}

export function doDelete(data) {
  return request({
    url: '/Biz/formDel',
    method: 'post',
    data,
  })
}

export function formInsAppv(data) {
  return request({
    url: '/Biz/formInsAppv',
    method: 'post',
    data,
  })
}

export function saveFormInstance(data) {
  return request({
    url: '/Biz/saveFormInstance',
    method: 'post',
    data,
  })
}
