import request from '@/utils/request'

export function getMessageTypes(data) {
  return request({
    url: '/Message/getMessageTypes',
    method: 'post',
    data,
  })
}

export function addMessageReciver(data) {
  return request({
    url: '/Message/addMessageReciver',
    method: 'post',
    data,
  })
}

export function removeMessageReciver(data) {
  return request({
    url: '/Message/removeMessageReciver',
    method: 'post',
    data,
  })
}

export function getMsgReciver(data) {
  return request({
    url: '/Message/getMsgReciver',
    method: 'post',
    data,
  })
}
